import os
import time
import yaml
import boto3
import logging
#from utils import *

class S3:
    def __init__(self, conf, retry=False):
        self.conf = conf
        self.retry = retry
        self.bucket_name = conf['bucket']
        self.image_prefix = conf['image_prefix']
        self.s3_client = self._connect()

    def _connect(self):
        num_retry = 0
        if self.retry:
            num_retry = 3
        while True:
            try:
                credential = yaml.load(open(self.conf['credential']))
                aws_access_key_id = credential['aws_access_key_id']
                aws_secret_access_key = credential['aws_secret_access_key']
                region_name = credential['region']
                s3_client = boto3.client('s3',
                                         region_name=region_name,
                                         aws_access_key_id=aws_access_key_id,
                                         aws_secret_access_key=aws_secret_access_key)
                return s3_client
            except Exception as e:
                logging.error("faild to create s3_client")
                logging.error(e)
                if num_retry > 0:
                    num_retry -= 1
                    logging.info("retry boto3 client creation")
                    time.sleep(1)
                else:
                    return None


    def upload(self, folder, src):
        """
        s3 upload function
        :param src: source file path
        :param type: 1 on source code, 2 on audit result
        :param retry: boolean, retry or not if upload failed. default is False, no retry
        :return: (0,s3_key) on success, (1,None) on error
        """
        src_filename = os.path.basename(src)
        s3_key = os.path.join(self.image_prefix, folder, src_filename)

        num_retry = 0
        if self.retry:
            num_retry = 3
        while True:
            try:
                self.s3_client.upload_file(src, self.bucket_name, s3_key)
                return 0, s3_key
            except Exception as e:
                logging.error("failed to upload {} to s3 bucket {} key {}".format(src, self.bucket_name, s3_key))
                logging.error(e)
                if num_retry > 0:
                    logging.info("Retry upload {}".format(num_retry))
                    num_retry -= 1

                    if self.s3_client is None:
                        logging.info("s3 client is None")
                        self.s3_client = self._connect()
                    time.sleep(1)
                else:
                    return 1, None

    def upload_bytes(self, folder, src_byte, filename):
        """
        s3 upload function
        :param src_byte: bytes of the object to be uploaded
        :param s3_key: s3 key
        :return: 0 on success, 1 on error
        """
        s3_key = os.path.join(self.image_prefix, folder, filename)
        
        num_retry = 0
        if self.retry:
            num_retry = 3
        while True:
            try:
                self.s3_client.put_object(Bucket=self.bucket_name, Key=s3_key, Body=src_byte)
                return 0, s3_key
            except Exception as e:
                logging.error("failed to upload bytes to s3 bucket {} key {}".format(self.bucket_name, s3_key))
                logging.error(e)
                if num_retry > 0:
                    logging.info("Retry upload {}".format(num_retry))
                    num_retry -= 1

                    if self.s3_client is None:
                        logging.info("s3 client is None")
                        self.s3_client = self._connect()
                    time.sleep(1)
                else:
                    return 1, None

    def download(self, s3_key):
        num_retry = 0
        if self.retry:
            num_retry = 3
        while True:
            try:
                s3_obj = self.s3_client.get_object(Bucket=self.bucket_name, Key=s3_key)
                return s3_obj['Body'].read()
            except Exception as e:
                logging.error("failed to download {} from s3 bucket {}".format(s3_key, self.bucket_name))
                logging.error(e)
                if num_retry > 0:
                    logging.info("Retry download {}".format(num_retry))
                    num_retry -= 1
                    logging.info("s3 client is None")
                    if self.s3_client is None:
                        self.s3_client = self._connect()
                    time.sleep(1)
                else:
                    return None



